package heroaddonsfo.entity.model;

import software.bernie.geckolib.model.GeoModel;

import net.minecraft.resources.ResourceLocation;

import heroaddonsfo.entity.CaveDwellerEntity;

public class CaveDwellerModel extends GeoModel<CaveDwellerEntity> {
	@Override
	public ResourceLocation getAnimationResource(CaveDwellerEntity entity) {
		return new ResourceLocation("heroaddonsfo", "animations/cave_dweller.animation.json");
	}

	@Override
	public ResourceLocation getModelResource(CaveDwellerEntity entity) {
		return new ResourceLocation("heroaddonsfo", "geo/cave_dweller.geo.json");
	}

	@Override
	public ResourceLocation getTextureResource(CaveDwellerEntity entity) {
		return new ResourceLocation("heroaddonsfo", "textures/entities/" + entity.getTexture() + ".png");
	}

}
