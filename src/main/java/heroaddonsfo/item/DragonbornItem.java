
package heroaddonsfo.item;

import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.world.item.RecordItem;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;
import net.minecraft.resources.ResourceLocation;

public class DragonbornItem extends RecordItem {
	public DragonbornItem() {
		super(0, () -> ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation("heroaddonsfo:dragonborn")), new Item.Properties().stacksTo(1).rarity(Rarity.COMMON), 1000);
	}
}
