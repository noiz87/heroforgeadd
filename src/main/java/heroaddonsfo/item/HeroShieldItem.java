
package heroaddonsfo.item;

import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.item.Item;

public class HeroShieldItem extends ShieldItem {
	public HeroShieldItem() {
		super(new Item.Properties().durability(100));
	}
}
