
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package heroaddonsfo.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.network.chat.Component;
import net.minecraft.core.registries.Registries;

import heroaddonsfo.HeroaddonsfoMod;

public class HeroaddonsfoModTabs {
	public static final DeferredRegister<CreativeModeTab> REGISTRY = DeferredRegister.create(Registries.CREATIVE_MODE_TAB, HeroaddonsfoMod.MODID);
	public static final RegistryObject<CreativeModeTab> HEROFORGE = REGISTRY.register("heroforge",
			() -> CreativeModeTab.builder().title(Component.translatable("item_group.heroaddonsfo.heroforge")).icon(() -> new ItemStack(HeroaddonsfoModItems.HERO_SHIELD.get())).displayItems((parameters, tabData) -> {
				tabData.accept(HeroaddonsfoModItems.HERO_SHIELD.get());
				tabData.accept(HeroaddonsfoModItems.DRAGONBORN.get());
				tabData.accept(HeroaddonsfoModItems.CAVE_DWELLER_SPAWN_EGG.get());
			})

					.build());
}
