
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package heroaddonsfo.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.resources.ResourceLocation;

import heroaddonsfo.HeroaddonsfoMod;

public class HeroaddonsfoModSounds {
	public static final DeferredRegister<SoundEvent> REGISTRY = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, HeroaddonsfoMod.MODID);
	public static final RegistryObject<SoundEvent> DRAGONBORN = REGISTRY.register("dragonborn", () -> SoundEvent.createVariableRangeEvent(new ResourceLocation("heroaddonsfo", "dragonborn")));
	public static final RegistryObject<SoundEvent> WELCOME = REGISTRY.register("welcome", () -> SoundEvent.createVariableRangeEvent(new ResourceLocation("heroaddonsfo", "welcome")));
}
