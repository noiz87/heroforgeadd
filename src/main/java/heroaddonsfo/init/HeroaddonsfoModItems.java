
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package heroaddonsfo.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.common.ForgeSpawnEggItem;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.item.Items;
import net.minecraft.world.item.Item;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.item.ItemProperties;

import heroaddonsfo.item.HeroShieldItem;
import heroaddonsfo.item.DragonbornItem;

import heroaddonsfo.HeroaddonsfoMod;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class HeroaddonsfoModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, HeroaddonsfoMod.MODID);
	public static final RegistryObject<Item> HERO_SHIELD = REGISTRY.register("hero_shield", () -> new HeroShieldItem());
	public static final RegistryObject<Item> DRAGONBORN = REGISTRY.register("dragonborn", () -> new DragonbornItem());
	public static final RegistryObject<Item> CAVE_DWELLER_SPAWN_EGG = REGISTRY.register("cave_dweller_spawn_egg", () -> new ForgeSpawnEggItem(HeroaddonsfoModEntities.CAVE_DWELLER, -1, -1, new Item.Properties()));

	// Start of user code block custom items
	// End of user code block custom items
	@SubscribeEvent
	public static void clientLoad(FMLClientSetupEvent event) {
		event.enqueueWork(() -> {
			ItemProperties.register(HERO_SHIELD.get(), new ResourceLocation("blocking"), ItemProperties.getProperty(Items.SHIELD, new ResourceLocation("blocking")));
		});
	}
}
