
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package heroaddonsfo.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import heroaddonsfo.client.renderer.CaveDwellerRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class HeroaddonsfoModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(HeroaddonsfoModEntities.CAVE_DWELLER.get(), CaveDwellerRenderer::new);
	}
}
